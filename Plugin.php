<?php

/*
Markdown Editor Plugin: Replaces the default code editor with a markdown
editor for md files in CMS/content 
Copyright (C) 2019 Balázs Dura-Kovács

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace Balping\MarkdownEditor;

use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Balping\MarkdownEditor\Models\Settings;

class Plugin extends PluginBase
{
	public function registerComponents(){

	}

	public function registerSettings(){
		return [
			'settings' => [
				'label' => 'balping.markdowneditor::lang.plugin.name',
				'description' => 'balping.markdowneditor::lang.settings.description',
				'icon' => 'icon-pencil-square-o',
				'class' => 'Balping\MarkdownEditor\Models\Settings',
				'category' => SettingsManager::CATEGORY_CMS,
				'permissions' => ['balping.markdowneditor.settings']
			]
		];
	}

	public function boot(){
		if(!Settings::get('replace', false)){
			return;
		}

		\Event::listen('backend.form.extendFields', function($widget) {

			if (!$widget->model instanceof \Cms\Classes\Content) {
				return;
			}

			$extension = strtolower(\File::extension($widget->model->fileName));

			if($extension !== 'md'){
				return;
			}

			$widget->getField('markup')->config['type'] = 'markdown';
			$widget->getField('markup')->config['widget'] = 'markdown';
			$widget->getField('markup')->config['mode'] = Settings::get('mode', 'split');
		});
	}
}
