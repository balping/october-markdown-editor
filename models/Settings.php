<?php

/*
Markdown Editor Plugin: Replaces the default code editor with a markdown
editor for md files in CMS/content 
Copyright (C) 2019 Balázs Dura-Kovács

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace Balping\MarkdownEditor\Models;

use Model;

class Settings extends Model {
	public $implement = ['System.Behaviors.SettingsModel'];

	public $settingsCode = 'balping_markdown_editor_Settings';

	public $settingsFields = 'fields.yaml';
}