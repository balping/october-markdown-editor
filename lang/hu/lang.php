<?php

/*
Markdown Editor Plugin: Replaces the default code editor with a markdown
editor for md files in CMS/content 
Copyright (C) 2019 Balázs Dura-Kovács

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

return [
	'plugin' => [
		'name' => 'Markdown Szerkesztő',
		'description' => 'Lecseréli az alapértelmezett kódszerkesztőt egy markdown szerkesztőre a CMS/tartalom alatt md file-ok esetén'
	],
	'form' => [
		'replace' => [
			'label' => 'Szerkesztő cseréje',
			'comment' => 'A plugin ki- és bekapcsolása',
		],
		'mode' => [
			'label' => 'Mód',
			'comment' => 'Előnézet gomb, vagy osztott nézet',
			'tab' => 'Előnézet gomb',
			'split' => 'Osztott képernyő',
		],
	],
	'settings' => [
		'description' => 'Beállítások a  Markdown Szerkesztő részére',
	],
];