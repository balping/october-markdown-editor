<?php

/*
Markdown Editor Plugin: Replaces the default code editor with a markdown
editor for md files in CMS/content 
Copyright (C) 2019 Balázs Dura-Kovács

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

return [
	'plugin' => [
		'name' => 'Markdown Editor',
		'description' => 'Replaces the default code editor with a markdown editor for md files in CMS/content'
	],
	'form' => [
		'replace' => [
			'label' => 'Replace editor',
			'comment' => 'Turns the plugin on and off',
		],
		'mode' => [
			'label' => 'Mode',
			'comment' => 'Editor mode: split screen or tab (preview button)',
			'tab' => 'Tab',
			'split' => 'Split screen',
		],
	],
	'settings' => [
		'description' => 'Settings for the Markdown Editor plugin',
	],
];